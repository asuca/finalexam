package finalexam;

public class SmsMessage extends Message {

	public SmsMessage(User receiver, User sender, String body) {
		super(receiver, sender, body);
	}
	
	public String toString() {
	SmsUser ruser = (SmsUser)super.getReceiver();
	SmsUser suser = (SmsUser)super.getSender();
	return "Receiver : " + "Name-" +getReceiver().getFirstName() 
			+ getReceiver().getLastName() + ", Address-"
			+ getReceiver().getAddress().getStreetAddress()+ getReceiver().getAddress().getBuildingNumber() 
			+ ", Phone Num- " + ruser.getPhoneNumber()
			+ ", Sender : " +  "Name-" +getSender().getFirstName() + getSender().getLastName()
			+ ", Address-" +getReceiver().getAddress().getStreetAddress()+ getReceiver().getAddress().getBuildingNumber() 			
			+ ", Phone Num- " + suser.getPhoneNumber()
			+ ", Body : " + super.getBody() +".";	
	}
}
