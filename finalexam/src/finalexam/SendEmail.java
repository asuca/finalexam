package finalexam;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendEmail implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		//email validation
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		EmailUser semail = (EmailUser)sender;
		EmailUser remail = (EmailUser)receiver;
		Pattern pattern = Pattern.compile(EMAIL_REGEX);
		Matcher semailVal = pattern.matcher(semail.getEmailAddress());
		Matcher remailVal = pattern.matcher(remail.getEmailAddress());
		if (!semailVal.matches() || !remailVal.matches()) {
			throw new IllegalArgumentException("Invalid EmailAddress");
		}
		//body check
		if(body.isBlank() || body.isEmpty() || body.contains("^") || body.contains("*")
				|| body.contains("!")){
			throw new IllegalArgumentException("Invalid body!");
		}
		return true;
	}

	@Override
	public void sendMessage(List<Message> message) {
		try {
			FileOutputStream   fop = new FileOutputStream("email.txt");
			OutputStreamWriter outPutWriter = new OutputStreamWriter(fop,"UTF-16");
			BufferedWriter  bufferWriter = new BufferedWriter(outPutWriter);
			for (Message str: message) {
				bufferWriter.write(str.toString());
				bufferWriter.newLine();
				bufferWriter.flush();
			}
			bufferWriter.close();
		}catch (IOException e){
			e.printStackTrace();
		}
	}	
}
