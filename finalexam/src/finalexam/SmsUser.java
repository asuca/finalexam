package finalexam;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsUser extends User {
	private String phoneNumber;
	
	public SmsUser(String firstName, String lastName, Address address,String phoneNumber) {
		super(firstName, lastName, address);
		this.phoneNumber = phoneNumber;
		if (!isValidatePhoneNumber()) {
			throw new IllegalArgumentException("Invalid Phone Number!");
		}	
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	//Phone Number Validation
	public boolean isValidatePhoneNumber() {
		//String regex = "(0/91)?[7-9][0-9]{9}";
		String regex = "^\\d{10}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher phoneNum = pattern.matcher(this.phoneNumber);
		return phoneNum.matches();
	}
}
