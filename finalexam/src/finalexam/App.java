package finalexam;

import java.util.ArrayList;
import java.util.List;

public class App {
	public static void main(String[] args) {
		//Create a list of message
		List<Message> listOfEMessages = new ArrayList<Message>();
		
		//Create an EmailMessage instance
		EmailMessage email1 = new EmailMessage(
				new EmailUser("Judy","Foster",new Address("MainStreet",1),"a.b@g.com"),
				new EmailUser("Betty","Beans",new Address("SecondStreet",2),"v.r@g.com"),
				"This is one email");
		
		EmailMessage email2 = new EmailMessage(
				new EmailUser("Jim","Kevin",new Address("ProvostStreet",1),"c.d@g.com"),
				new EmailUser("John","Toms",new Address("VoctoryStreet",2),"h.j@g.com"),
				"This is one email");
		//Write to txt file
		listOfEMessages.add(email1);
		listOfEMessages.add(email2);
		SendEmail sendEmail = new SendEmail();
		sendEmail.sendMessage(listOfEMessages);
		
		
		//Create a list of message
		List<Message> listOfSMessages = new ArrayList<Message>();
		//Create an SmsMessage instance
		SmsMessage smsMessage1 = new SmsMessage(
				new SmsUser("Judy","Foster",new Address("MainStreet",1),"1221231231"),
				new SmsUser("Betty","Beans",new Address("SecondStreet",2),"1221231231"),
				"This is one sms");
		SmsMessage smsMessage2 = new SmsMessage(
				new SmsUser("Jim","Kevin",new Address("ProvostStreet",1),"1221231211"),
				new SmsUser("John","Toms",new Address("VoctoryStreet",2),"1221231231"),
				"This is one sms");
		//Write to txt file
		listOfSMessages.add(smsMessage1);
		listOfSMessages.add(smsMessage2);
		SendSms sendSMS = new SendSms();
		sendSMS.sendMessage(listOfSMessages);
	}
}
