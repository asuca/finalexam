package finalexam;

public class EmailMessage extends Message {

	public EmailMessage(User receiver, User sender, String body) {
		super(receiver, sender, body);
	}
	public String toString() {
	EmailUser ruser = (EmailUser)super.getReceiver();
	EmailUser suser = (EmailUser)super.getSender();
	return "Receiver : " + "Name-" +getReceiver().getFirstName() 
			+ getReceiver().getLastName() + ", Address-"
			+ getReceiver().getAddress().getStreetAddress()+ getReceiver().getAddress().getBuildingNumber() 
			+ ", EmailId- " + ruser.getEmailAddress() 
			+ ", Sender : " +  "Name-" +getSender().getFirstName() + getSender().getLastName()
			+ ", Address-" +getReceiver().getAddress().getStreetAddress()+ getReceiver().getAddress().getBuildingNumber() 			
			+ ", EmailId- " + suser.getEmailAddress()
			+ ", Body : " + super.getBody() +".";	
	}
}
