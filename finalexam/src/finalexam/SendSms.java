package finalexam;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class SendSms implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		SmsUser sUser = (SmsUser)sender;
		SmsUser rUser = (SmsUser)receiver;
		if(sUser.getPhoneNumber().length() != 10 || rUser.getPhoneNumber().length() != 10) {
			throw new IllegalArgumentException("Invalid phone number!");
		}
		if(body.isBlank() || body.isEmpty() || body.length() > 160 || body.contains("&") || 
				body.contains("#") || body.contains("@")){
			throw new IllegalArgumentException("Invalid phone number!");
		}
		return true;
	}

	@Override
	public void sendMessage(List<Message> message) {
		try {
			FileOutputStream   fop = new FileOutputStream("sms.txt");
			OutputStreamWriter outPutWriter = new OutputStreamWriter(fop,"UTF-16");
			BufferedWriter  bufferWriter = new BufferedWriter(outPutWriter);
			for (Message str: message) {
				bufferWriter.write(str.toString());
				bufferWriter.newLine();
				bufferWriter.flush();
			}
			bufferWriter.close();
		}catch (IOException e){
			e.printStackTrace();
		}
	}	
}
