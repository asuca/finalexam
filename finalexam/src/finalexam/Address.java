package finalexam;

public class Address {
	private String streetAddress;
	private int buildingNumber;
	//constructor
	public Address(String streetAddress,int buildingNumber) {
		this.streetAddress  = streetAddress;
		this.buildingNumber = buildingNumber;
	}
	//getter
	public int getBuildingNumber() {
		return buildingNumber;
	}
	public String getStreetAddress() {
		return streetAddress;
	}	
}
