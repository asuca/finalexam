package finalexam;

import java.util.List;

public interface ISendInfo {
	boolean validateMessage(User sender, User receiver, String body) ;
	void sendMessage(List<Message> message);
}
