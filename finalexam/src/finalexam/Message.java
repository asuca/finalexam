package finalexam;

public class Message {
	private User receiver;
	private User sender;
	private String body;
	
	public Message(User receiver, User sender, String body) {
		this.receiver = receiver;
		this.sender   = sender;
		this.body     = body;
	}

	public User getReceiver() {
		return receiver;
	}

	public User getSender() {
		return sender;
	}

	public String getBody() {
		return body;
	}
	
//	public String toString() {
//		EmailUser ruser = (EmailUser)receiver;
//		EmailUser suser = (EmailUser)sender;
//		return "Receiver : " + "Name-" +getReceiver().getFirstName() 
//				+ getReceiver().getLastName() + ", Address-"
//				+ getReceiver().getAddress().getStreetAddress()+ getReceiver().getAddress().getBuildingNumber() 
//				+ ", EmailId- " + ruser.getEmailAddress() 
//				+ ", Sender : " +  "Name-" +getSender().getFirstName() + getSender().getLastName()
//				+ ", Address-" +getReceiver().getAddress().getStreetAddress()+ getReceiver().getAddress().getBuildingNumber() 			
//				+ ", EmailId- " + suser.getEmailAddress()
//				+ ", Body : " + body +".";	
//	}
}
